package main

import (
	_ "log"
)

func runReworkPercentVaccin() {
	percentVaccine.calculPercentByMonth()

}

func (monthPercent AllPercentVaccine) calculPercentByMonth() {
	var fistDosePercent, secondDosePercent, thirstDosePercent float64 = 0, 0, 0
	month := monthPercent[0].DateGo.Month()
	firstIdx := 0
	count := 0
	for index, value := range monthPercent {
		if value.DateGo.Month() != month {

			monthPercent.updatePercent(fistDosePercent, secondDosePercent, thirstDosePercent, count, firstIdx, index-1)

			fistDosePercent, secondDosePercent, thirstDosePercent = 0, 0, 0
			firstIdx = index
			count = 0
			month = value.DateGo.Month()
		}
		count++
		fistDosePercent += value.CumVaccinationFirstDoseUptakeByPublishDatePercentage
		secondDosePercent += value.CumVaccinationSecondDoseUptakeByPublishDatePercentage
		thirstDosePercent += value.CumVaccinationThirdInjectionUptakeByPublishDatePercentage

	}
	monthPercent.updatePercent(fistDosePercent, secondDosePercent, thirstDosePercent, count, firstIdx, len(monthPercent)-1)
}

func (monthPercent AllPercentVaccine) updatePercent(fistDosePercent, secondDosePercent, thirstDosePercent float64, diviseur, firstIdx, lastIdx int) {
	for index := firstIdx; index <= lastIdx; index++ {
		monthPercent[index].MonthPercentFirstDose = fistDosePercent / float64(diviseur)
		monthPercent[index].MonthPercentSecondDose = secondDosePercent / float64(diviseur)
		monthPercent[index].MonthPercentThirstDose = thirstDosePercent / float64(diviseur)
	}
}
