package main

import (
	"bufio"
	"encoding/csv"
	"io"
	"os"
	"time"

	"github.com/gocarina/gocsv"
)

var (
	ageDemog         AgeDemog
	demog            map[string]map[string]float64 = make(map[string]map[string]float64)
	mapAgeDemogToOns                               = map[string]string{
		"12_15": "10-39",
		"16_17": "10-39",
		"18_24": "10-39",
		"25_29": "10-39",
		"30_34": "10-39",
		"35_39": "10-39",
		"40_44": "40-49",
		"45_49": "40-49",
		"50_54": "50-59",
		"55_59": "50-59",
		"60_64": "60-69",
		"65_69": "60-69",
		"70_74": "70-79",
		"75_79": "70-79",
		"80_84": "80-89",
		"85_89": "80-89",
		"90+":   "90+",
	}
)

type AgeDemog []VacinneByAge

type VacinneByAge struct {
	AreaCode                                                 string `csv:"areaCode"`
	AreaName                                                 string `csv:"areaName"`
	AreaType                                                 string `csv:"areaType"`
	Date                                                     string `csv:"date"`
	DateGo                                                   time.Time
	Age                                                      string  `csv:"age"`
	OneAgeGroupPercent                                       float64 `csv:"Ons Age Group Percent"`
	VaccineRegisterPopulationByVaccinationDate               int     `csv:"VaccineRegisterPopulationByVaccinationDate"`
	CumPeopleVaccinatedCompleteByVaccinationDate             int     `csv:"cumPeopleVaccinatedCompleteByVaccinationDate"`
	NewPeopleVaccinatedCompleteByVaccinationDate             int     `csv:"newPeopleVaccinatedCompleteByVaccinationDate"`
	CumPeopleVaccinatedFirstDoseByVaccinationDate            int     `csv:"cumPeopleVaccinatedFirstDoseByVaccinationDate"`
	NewPeopleVaccinatedFirstDoseByVaccinationDate            int     `csv:"newPeopleVaccinatedFirstDoseByVaccinationDate"`
	CumPeopleVaccinatedSecondDoseByVaccinationDate           int     `csv:"cumPeopleVaccinatedSecondDoseByVaccinationDate"`
	NewPeopleVaccinatedSecondDoseByVaccinationDate           int     `csv:"newPeopleVaccinatedSecondDoseByVaccinationDate"`
	CumPeopleVaccinatedThirdInjectionByVaccinationDate       int     `csv:"cumPeopleVaccinatedThirdInjectionByVaccinationDate"`
	NewPeopleVaccinatedThirdInjectionByVaccinationDate       int     `csv:"newPeopleVaccinatedThirdInjectionByVaccinationDate"`
	CumVaccinationFirstDoseUptakeByVaccinationDatePercentage float64 `csv:"cumVaccinationFirstDoseUptakeByVaccinationDatePercentage"`
	CumVaccinationFirstDoseUptakeByPublishDatePercentage     float64 `csv:"cumVaccinationFirstDoseUptakeByPublishDatePercentage"`
}

func loadAgeDemographic() {
	gocsv.SetCSVReader(func(in io.Reader) gocsv.CSVReader {
		r := csv.NewReader(in)
		r.Comma = ';'
		return r
	})
	csvFile, err := os.Open("input/age_demographique.csv")
	printErr(err)
	defer csvFile.Close()
	bufio.NewReader(csvFile)
	gocsv.Unmarshal(csvFile, &ageDemog)

	for index := range ageDemog {
		t, _ := time.Parse("02/01/2006", ageDemog[index].Date)
		ageDemog[index].DateGo = t
	}
}

func saveAgeDemographic() {
	csvWrite, err := os.OpenFile("output/rework_age_demographique.csv", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0655)
	printErr(err)
	defer csvWrite.Close()
	writeThis, err := gocsv.MarshalBytes(&ageDemog)
	printErr(err)
	csvWrite.Write(writeThis)
}
