package main

import (
	"bufio"
	"encoding/csv"
	"io"
	"os"
	"time"

	"github.com/gocarina/gocsv"
)

var (
	percentVaccine AllPercentVaccine
)

type AllPercentVaccine []PercentVaccine

type PercentVaccine struct {
	AreaType                                                  string `csv:"areaType"`
	AreaName                                                  string `csv:"areaName"`
	AreaCode                                                  string `csv:"areaCode"`
	Date                                                      string `csv:"date"`
	DateGo                                                    *time.Time
	CumVaccinationFirstDoseUptakeByVaccinationDatePercentage  float64 `csv:"cumVaccinationFirstDoseUptakeByVaccinationDatePercentage"`
	CumVaccinationFirstDoseUptakeByPublishDatePercentage      float64 `csv:"cumVaccinationFirstDoseUptakeByPublishDatePercentage"`
	CumVaccinationSecondDoseUptakeByPublishDatePercentage     float64 `csv:"cumVaccinationSecondDoseUptakeByPublishDatePercentage"`
	CumVaccinationThirdInjectionUptakeByPublishDatePercentage float64 `csv:"cumVaccinationThirdInjectionUptakeByPublishDatePercentage"`
	MonthPercentFirstDose                                     float64 `csv:"Month Percent First Dose"`
	MonthPercentSecondDose                                    float64 `csv:"Month Percent Seconde Dose"`
	MonthPercentThirstDose                                    float64 `csv:"Month Percent Thirst Dose"`
}

func loadPercentVaccine() {
	gocsv.SetCSVReader(func(in io.Reader) gocsv.CSVReader {
		r := csv.NewReader(in)
		r.Comma = ','
		return r // Allows use pipe as delimiter
	})
	csvFile, err := os.Open("input/vaccine_percent_2022-Aug-17.csv")
	printErr(err)
	defer csvFile.Close()
	bufio.NewReader(csvFile)
	gocsv.Unmarshal(csvFile, &percentVaccine)
	for index := range percentVaccine {
		t, _ := time.Parse("2006-01-02", percentVaccine[index].Date)
		percentVaccine[index].DateGo = &t
	}
}

func savePercentVaccine() {
	csvWrite, err := os.OpenFile("output/rework_vaccine_percent.csv", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0655)
	printErr(err)
	defer csvWrite.Close()
	writeThis, err := gocsv.MarshalBytes(&percentVaccine)
	printErr(err)
	csvWrite.Write(writeThis)
}
