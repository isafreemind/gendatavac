package main

func main() {
	// Loading File
	loadONSXls()
	loadAgeDemographic()
	loadPercentVaccine()

	// Traitement
	runReworkAgeDemog()
	addNoCovidDeathCause()
	runReworkPercentVaccin()
	runReworkOns()

	// Save files
	savePercentVaccine()
	saveAgeDemographic()
	saveONS()
	saveTable6()
	saveTable9()
}
