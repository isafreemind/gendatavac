package main

import (
	"bytes"
	"log"
	"os"
	"strings"
)

func writeExcelFormat(b []byte, path string) {
	output1 := bytes.Replace(b, []byte(","), []byte(";"), -1)
	output2 := bytes.Replace(output1, []byte("."), []byte(","), -1)

	if err := os.WriteFile(strings.Replace(path, ".csv", "_other_format.csv", -1), output2, 0666); err != nil {
		log.Fatal(err)
	}
}
