package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/gocarina/gocsv"
	"github.com/xuri/excelize/v2"
)

var (
	onsData DataOns
)

type DataOns struct {
	Table6 []Table6
	Table8 []Table8
	Table9 []Table9
}

type Table8 struct {
	CauseOfDeath                      string     `csv:"Cause of Death"`
	Year                              int        `csv:"Year"`
	Month                             string     `csv:"Month"`
	DateGo                            *time.Time `csv:",omitempty"`
	AgeGroup                          string     `csv:"Age group"`
	VaccinationStatus                 string     `csv:"Vaccination status"`
	CountOfDeaths                     int        `csv:"Count of Deaths"`
	NormalizedDeathsCountOfDeaths     float64    `csv:"Normalized Deaths Count Of Deaths"`
	MonthPercentVaccinAge             float64    `csv:"Month Percent Vaccine Age"`
	AbsoluteDeathsStandardizedByAge   float64    `csv:"Absolute Deaths Standardized By Age"`
	ForCentDeathUnvaccinated          float64    `csv:"For Cent Death Unvaccinated"`
	DeathForOneUnvaccinated           float64    `csv:"Death For One Unvaccinated"`
	DeathForOneUnvaccinatedAbsolute   float64    `csv:"Death For One Unvaccinated Absolute"`
	CompareDeathPercent               float64    `csv:"Compare Death Percent"`
	MonthTotalDeathsByAge             int        `csv:"Month Total Deaths By Age"`
	AbsoluteDeathForOneUnvaccined     float64    `csv:"Absolute Death For One Unvaccined"`
	PercentDeathVaccintedMonthByAge   float64    `csv:"Percent Death Vaccinted Month By Age"`
	PercentDeathUnVaccintedMonthByAge float64    `csv:"Percent Death Unvaccinted Month By Age"`
	MonthTotalDeath                   int        `csv:"Month Total Death"`
	MonthTotalDeathsByStatus          int        `csv:"Month Total Deaths By Status"`
	MonthPercentDeathsByStatus        float64    `csv:"Month Percent Deaths By Status"`
	MonthPercentFirstDose             float64    `csv:"Month Percent First Dose"`
	MonthPercentSecondDose            float64    `csv:"Month Percent Seconde Dose"`
	MonthPercentThirstDose            float64    `csv:"Month Percent Thirst Dose"`
	MonthRatioDeathsByStatus          float64    `csv:"Month Ratio Deaths By Status"`
	RatioDeathsByStatusByAge          float64    `csv:"Ratio Deaths By Status By Age"`
	PercentRatioDeathsByStatusByAge   float64    `csv:"Percent Ratio Deaths By Status By Age"`
}

type Table6 struct {
	AgeGroup                             string  `csv:"Age group"`
	VaccinationStatus                    string  `csv:"Vaccination status"`
	PersonYears                          int     `csv:"Person-years"`
	CountOfDeathsInvolvingCOVID19        int     `csv:"Count of deaths involving COVID-19"`
	CountOfDeathsNonCOVID19Deaths        int     `csv:"Count of deaths non-COVID-19 deaths"`
	CountOfAllCauseDeaths                int     `csv:"Count of all cause deaths"`
	CompareDeathPercent                  float64 `csv:"Compare Death Percent"`
	VaccinatedPercent                    float64 `csv:"Vaccinated Percent"`
	StandardizedNoCovidDeathUnVaccinated float64 `csv:"Standardized UnVaccinated"`
	StandardizedCovidDeathVaccinated     float64 `csv:"Standardized Vaccinated"`
	TotalPersonYearsVaccinated           int     `csv:"Total PersonYears Vaccinated"`
	TotalPersonYearsUnVaccinated         int     `csv:"Total PersonYears UnVaccinated"`
	TotalDeathVaccinated                 int     `csv:"Total Death Vaccinated"`
	TotalDeathUnVaccinated               int     `csv:"Total Death UnVaccinated"`
	TotalDeathVaccinatedCovid            int     `csv:"Total Death Vaccinated Covid"`
	TotalDeathUnVaccinatedCovid          int     `csv:"Total Death UnVaccinated Covid"`
	TotalDeathVaccinatedNonCovid         int     `csv:"Total Death Vaccinated Non Covid"`
	TotalDeathUnVaccinatedNonCovid       int     `csv:"Total Death UnVaccinated Non Covid"`
}

type Table9 struct {
	WeekAfterVaccination          int     `csv:"Week after vaccination"`
	AgeGroup                      string  `csv:"Age group"`
	CountOfDeathsInvolvingCOVID19 int     `csv:"Count of Deaths involving COVID-19"`
	CountOfNonCOVID19Deaths       int     `csv:"Count of Non-COVID-19 Deaths"`
	NormalizedDeathsCountOfDeaths float64 `csv:"Normalized Deaths Count Of Deaths"`
	MonthPercentVaccinAge         float64 `csv:"Month Percent Vaccine Age"`
}

func saveONS() {
	path := "output/rework_ons.csv"
	csvWrite, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0655)
	printErr(err)
	defer csvWrite.Close()
	writeThis, err := gocsv.MarshalBytes(&onsData.Table8)
	printErr(err)
	csvWrite.Write(writeThis)
	writeExcelFormat(writeThis, path)
}

func printErr(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func loadONSXls() {
	onsXlsFile, err := excelize.OpenFile("input/referencetable06072022accessible.xlsx")
	if err != nil {
		fmt.Println(err)
		return
	}
	loadOnsTable6(onsXlsFile)
	loadOnsTable8(onsXlsFile)
	loadOnsTable9(onsXlsFile)
	defer func() {
		// Close the spreadsheet.
		if err := onsXlsFile.Close(); err != nil {
			fmt.Println(err)
		}
	}()
}

func loadOnsTable6(onsXlsFile *excelize.File) {
	// Get all the rows in the Sheet1.
	rows, err := onsXlsFile.GetRows("Table 6")
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, row := range rows[4:] {
		tmpList := Table6{}
		tmpList.AgeGroup = row[0]
		tmpList.VaccinationStatus = row[1]
		tmpList.PersonYears, _ = strconv.Atoi(row[2])
		tmpList.CountOfDeathsInvolvingCOVID19, _ = strconv.Atoi(row[3])
		tmpList.CountOfDeathsNonCOVID19Deaths, _ = strconv.Atoi(row[4])
		tmpList.CountOfAllCauseDeaths, _ = strconv.Atoi(row[5])
		onsData.Table6 = append(onsData.Table6, tmpList)
	}

	reqBodyBytes := new(bytes.Buffer)
	json.NewEncoder(reqBodyBytes).Encode(&onsData.Table6)
	gocsv.UnmarshalBytes(reqBodyBytes.Bytes(), &onsData.Table6)
}

func loadOnsTable8(onsXlsFile *excelize.File) {
	// Get all the rows in the Sheet1.
	rows, err := onsXlsFile.GetRows("Table 8")
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, row := range rows[4:] {
		tmpList := Table8{}
		tmpList.CauseOfDeath = row[0]
		tmpList.Year, _ = strconv.Atoi(row[1])
		tmpList.Month = row[2]
		tmpList.AgeGroup = row[3]
		tmpList.VaccinationStatus = row[4]
		tmpList.CountOfDeaths = correctInt(row[5], "Table 8")
		onsData.Table8 = append(onsData.Table8, tmpList)
	}

	reqBodyBytes := new(bytes.Buffer)
	json.NewEncoder(reqBodyBytes).Encode(&onsData.Table8)
	gocsv.UnmarshalBytes(reqBodyBytes.Bytes(), &onsData.Table8)

	for index := range onsData.Table8 {
		t, _ := time.Parse("2006 January", fmt.Sprintf("%d %s", onsData.Table8[index].Year, onsData.Table8[index].Month))
		onsData.Table8[index].DateGo = &t
	}
}

func loadOnsTable9(onsXlsFile *excelize.File) {
	// Get all the rows in the Sheet1.
	rows, err := onsXlsFile.GetRows("Table 9")
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, row := range rows[4:] {
		tmpList := Table9{}
		tmpList.WeekAfterVaccination, _ = strconv.Atoi(row[0])
		tmpList.AgeGroup = row[1]
		tmpList.CountOfDeathsInvolvingCOVID19 = correctInt(row[2], "Table 9")
		tmpList.CountOfNonCOVID19Deaths, _ = strconv.Atoi(row[3])
		onsData.Table9 = append(onsData.Table9, tmpList)
	}

	reqBodyBytes := new(bytes.Buffer)
	json.NewEncoder(reqBodyBytes).Encode(&onsData.Table9)
	gocsv.UnmarshalBytes(reqBodyBytes.Bytes(), &onsData.Table9)
}

func correctInt(value, table string) int {
	if value == "<3" {
		return 2
	}

	v, err := strconv.Atoi(value)
	if err == nil {
		return v
	} else {
		log.Fatalf("Error: %s. Table: %s", value, table)
	}
	return 0
}

func saveTable9() {
	path := "output/table_9.csv"
	csvWrite, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0655)
	printErr(err)
	defer csvWrite.Close()
	writeThis, err := gocsv.MarshalBytes(&onsData.Table9)
	printErr(err)
	csvWrite.Write(writeThis)
	writeExcelFormat(writeThis, path)
}

func saveTable6() {
	path := "output/table_6.csv"
	csvWrite, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0655)
	printErr(err)
	defer csvWrite.Close()
	writeThis, err := gocsv.MarshalBytes(&onsData.Table6)
	printErr(err)
	csvWrite.Write(writeThis)
	writeExcelFormat(writeThis, path)
}
