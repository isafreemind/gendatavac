package main

import (
	"log"
	"strconv"
)

func runReworkOns() {
	onsData.workTable9()
	onsData.calculDesTotaux()

}

func addNoCovidDeathCause() {
	var noCovidAdd []Table8
	for _, val := range onsData.Table8 {
		if val.CauseOfDeath == "All causes" {
			noCovidAdd = append(noCovidAdd, val.getDifferenceNoCovidCause())
		}
	}
	onsData.Table8 = append(onsData.Table8, noCovidAdd...)
}

func (ons *DataOns) calculDesTotaux() {
	var monthUnVacTotalDeaths, monthVacTotalDeaths, totalDeVacAge, totalDeathUnVacAge int = 0, 0, 0, 0
	var month string = ons.Table8[0].Month
	var ageTotalDeath int = 0
	var ageDeathIndex []int
	age := ons.Table8[0].AgeGroup
	firstIdx := 0
	for index, value := range ons.Table8 {
		if age != value.AgeGroup {
			updateTotalDeathByAge(&ageDeathIndex, &ageTotalDeath, &totalDeathUnVacAge, &totalDeVacAge, &age, &value.AgeGroup)
		}

		key := strconv.Itoa(value.DateGo.Year()) + value.DateGo.Month().String() + value.AgeGroup
		ons.Table8[index].MonthPercentVaccinAge = demog[key]["percent"]

		if value.Month != month {

			ons.updateOnsDate(monthUnVacTotalDeaths, monthVacTotalDeaths, firstIdx, index-1)

			monthUnVacTotalDeaths, monthVacTotalDeaths = 0, 0
			firstIdx = index
			month = value.Month
		}

		ageTotalDeath += value.CountOfDeaths
		ageDeathIndex = append(ageDeathIndex, index)

		if value.VaccinationStatus == "Unvaccinated" {
			ons.Table8[index].AbsoluteDeathsStandardizedByAge = float64(ons.Table8[index].CountOfDeaths)
			monthUnVacTotalDeaths += value.CountOfDeaths
			totalDeathUnVacAge += value.CountOfDeaths
		} else {
			monthVacTotalDeaths += value.CountOfDeaths
			totalDeVacAge += value.CountOfDeaths
			ons.Table8[index].AbsoluteDeathsStandardizedByAge = float64(float64(ons.Table8[index].CountOfDeaths) * (100 - demog[key]["percent"]/demog[key]["percent"]))
		}

	}
	updateTotalDeathByAge(&ageDeathIndex, &ageTotalDeath, &totalDeathUnVacAge, &totalDeVacAge, &age, &age)
	ons.updateOnsDate(monthUnVacTotalDeaths, monthVacTotalDeaths, firstIdx, len(ons.Table8)-1)
}

func updateTotalDeathByAge(ageDeathIndex *[]int, ageTotalDeath, totalDeathUnVacAge, totalDeathVacAge *int, age *string, newAge *string) {
	for _, i := range *ageDeathIndex {
		line := &onsData.Table8[i]
		line.MonthTotalDeathsByAge = *ageTotalDeath
		line.PercentDeathUnVaccintedMonthByAge = float64(*totalDeathUnVacAge) / float64(*ageTotalDeath) * 100
		line.PercentDeathVaccintedMonthByAge = float64(*totalDeathVacAge) / float64(*ageTotalDeath) * 100
		line.DeathForOneUnvaccinated = float64(*totalDeathVacAge) / float64(*totalDeathUnVacAge)
	}
	*ageDeathIndex = []int{}
	*ageTotalDeath = 0
	*totalDeathUnVacAge = 0
	*totalDeathVacAge = 0
	*age = *newAge
}

func (ons *DataOns) updateOnsDate(monthUnVacTotalDeaths, monthVacTotalDeaths, firstIdx, lastIdx int) {
	totalDeath := monthUnVacTotalDeaths + monthVacTotalDeaths
	monthPercentUnVacDeaths := float64(monthUnVacTotalDeaths) / float64(totalDeath) * 100
	monthPercentVacDeaths := float64(monthVacTotalDeaths) / float64(totalDeath) * 100

	for index := firstIdx; index <= lastIdx; index++ {
		line := &ons.Table8[index]
		line.MonthTotalDeath = totalDeath

		if line.VaccinationStatus == "Unvaccinated" {
			line.MonthTotalDeathsByStatus = monthUnVacTotalDeaths
			line.MonthPercentDeathsByStatus = monthPercentUnVacDeaths
		} else {
			line.MonthTotalDeathsByStatus = monthVacTotalDeaths
			line.MonthPercentDeathsByStatus = monthPercentVacDeaths

		}

		first, second, thirst, monthRatioDeathsUnVaccinated, monthRatioDeathsVaccinated := line.getPercentMonthVaccine()

		line.MonthPercentFirstDose = first
		line.MonthPercentSecondDose = second
		line.MonthPercentThirstDose = thirst

		percentVacByAge := line.MonthPercentVaccinAge
		if line.VaccinationStatus == "Unvaccinated" {
			line.MonthRatioDeathsByStatus = monthRatioDeathsUnVaccinated
			line.RatioDeathsByStatusByAge = line.PercentDeathUnVaccintedMonthByAge / (100 - percentVacByAge)
		} else {
			line.MonthRatioDeathsByStatus = monthRatioDeathsVaccinated
			line.RatioDeathsByStatusByAge = line.PercentDeathVaccintedMonthByAge / (percentVacByAge)
		}

		line.normalyseOneHundredThousand(index)
		line.getPercentRatioDeathsByStatusByAge(index)
	}
}
func (line *Table8) getPercentRatioDeathsByStatusByAge(index int) {
	if line.VaccinationStatus == "Ever vaccinated" {
		sommeRatio := line.RatioDeathsByStatusByAge + onsData.Table8[index-1].RatioDeathsByStatusByAge
		line.PercentRatioDeathsByStatusByAge = line.RatioDeathsByStatusByAge * 100 / sommeRatio
		onsData.Table8[index-1].PercentRatioDeathsByStatusByAge = onsData.Table8[index-1].RatioDeathsByStatusByAge * 100 / sommeRatio
	}
}

func (line *Table8) normalyseOneHundredThousand(index int) {
	if line.VaccinationStatus == "Ever vaccinated" {
		onsData.Table8[index-1].AbsoluteDeathForOneUnvaccined = 1
		line.AbsoluteDeathForOneUnvaccined = line.AbsoluteDeathsStandardizedByAge / onsData.Table8[index-1].AbsoluteDeathsStandardizedByAge
		onsData.Table8[index-1].NormalizedDeathsCountOfDeaths = 100 / (100 - onsData.Table8[index-1].MonthPercentVaccinAge) * float64(onsData.Table8[index-1].CountOfDeaths)
		line.NormalizedDeathsCountOfDeaths = 100 / (line.MonthPercentVaccinAge) * float64(line.CountOfDeaths)
		onsData.Table8[index-1].DeathForOneUnvaccinated = 1
		line.DeathForOneUnvaccinated = line.NormalizedDeathsCountOfDeaths / onsData.Table8[index-1].NormalizedDeathsCountOfDeaths
		onsData.Table8[index-1].DeathForOneUnvaccinatedAbsolute = onsData.Table8[index-1].NormalizedDeathsCountOfDeaths * (100 - onsData.Table8[index-1].MonthPercentVaccinAge)
		line.DeathForOneUnvaccinatedAbsolute = line.NormalizedDeathsCountOfDeaths * (100 - onsData.Table8[index-1].MonthPercentVaccinAge)
		onsData.Table8[index-1].CompareDeathPercent = 0
		onenHundredRatio := 100 / onsData.Table8[index-1].NormalizedDeathsCountOfDeaths
		onsData.Table8[index-1].ForCentDeathUnvaccinated = 100
		noVacDeath := onsData.Table8[index-1].NormalizedDeathsCountOfDeaths
		vacDeath := line.NormalizedDeathsCountOfDeaths
		totalDeath := noVacDeath + vacDeath
		noVacPercentDeath := noVacDeath / (totalDeath) * 100
		vacPercentDeath := vacDeath / (totalDeath) * 100
		line.CompareDeathPercent = (vacPercentDeath - noVacPercentDeath) / noVacPercentDeath
		line.ForCentDeathUnvaccinated = onenHundredRatio * line.NormalizedDeathsCountOfDeaths
	}
}

func (key Table8) getPercentMonthVaccine() (first, second, thirst, monthRatioDeathsUnVaccinated, monthRatioDeathsVaccinated float64) {
	for _, value := range percentVaccine {
		if value.DateGo.Equal(*key.DateGo) {
			monthRatioDeathsUnVaccinated := key.MonthPercentDeathsByStatus / (100 - key.MonthPercentVaccinAge)
			monthRatioDeathsVaccinated := key.MonthPercentDeathsByStatus / key.MonthPercentVaccinAge
			return value.MonthPercentFirstDose,
				value.MonthPercentSecondDose,
				value.MonthPercentThirstDose,
				monthRatioDeathsUnVaccinated,
				monthRatioDeathsVaccinated
		}
	}
	return 0, 0, 0, 0, 0
}

func (allCauses Table8) getDifferenceNoCovidCause() Table8 {
	addThis := Table8{}
	for _, value := range onsData.Table8 {
		addThis = value
		if value.AgeGroup == allCauses.AgeGroup &&
			value.DateGo.Equal(*allCauses.DateGo) && value.VaccinationStatus == allCauses.VaccinationStatus && value.CauseOfDeath != allCauses.CauseOfDeath {
			addThis.CauseOfDeath = "Deaths no COVID-19"
			addThis.CountOfDeaths = allCauses.CountOfDeaths - value.CountOfDeaths
			return addThis
		}
	}
	return addThis
}

func (ons *DataOns) workTable9() {
	var export map[string]map[string]Table6 = make(map[string]map[string]Table6) // map => age => Table9
	t6 := Table6{}
	currentGroup := ons.Table6[0].AgeGroup
	for _, value := range ons.Table6 {
		if currentGroup != value.AgeGroup {
			t6 = Table6{}
		}
		currentGroup = value.AgeGroup
		if _, ok := export[value.AgeGroup]; !ok {
			export[value.AgeGroup] = map[string]Table6{}
			export[value.AgeGroup]["Unvaccinated"] = Table6{}
			export[value.AgeGroup]["Vaccinated"] = Table6{}
		}
		if value.VaccinationStatus != "Unvaccinated" {
			t6.PersonYears += value.PersonYears
			t6.TotalDeathVaccinated += value.CountOfAllCauseDeaths
			t6.TotalDeathVaccinatedNonCovid += value.CountOfDeathsNonCOVID19Deaths
			t6.TotalDeathVaccinatedCovid += value.CountOfDeathsInvolvingCOVID19
			export[value.AgeGroup]["Vaccinated"] = t6
		} else {
			t6.PersonYears += value.PersonYears
			t6.TotalDeathUnVaccinated += value.CountOfAllCauseDeaths
			t6.TotalDeathUnVaccinatedNonCovid += value.CountOfDeathsNonCOVID19Deaths
			t6.TotalDeathUnVaccinatedCovid += value.CountOfDeathsInvolvingCOVID19
			export[value.AgeGroup]["Unvaccinated"] = t6
		}
	}

	for index := range ons.Table6 {
		line := &ons.Table6[index]
		line.TotalPersonYearsVaccinated = export[line.AgeGroup]["Vaccinated"].PersonYears
		line.TotalDeathVaccinated = export[line.AgeGroup]["Vaccinated"].TotalDeathVaccinated
		line.TotalDeathVaccinatedNonCovid = export[line.AgeGroup]["Vaccinated"].TotalDeathVaccinatedNonCovid
		line.TotalDeathVaccinatedCovid = export[line.AgeGroup]["Vaccinated"].TotalDeathVaccinatedCovid

		line.TotalPersonYearsUnVaccinated = export[line.AgeGroup]["Unvaccinated"].PersonYears
		line.TotalDeathUnVaccinated = export[line.AgeGroup]["Unvaccinated"].TotalDeathUnVaccinated
		line.TotalDeathUnVaccinatedNonCovid = export[line.AgeGroup]["Unvaccinated"].TotalDeathUnVaccinatedNonCovid
		line.TotalDeathUnVaccinatedCovid = export[line.AgeGroup]["Unvaccinated"].TotalDeathUnVaccinatedCovid

		line.VaccinatedPercent = float64(line.TotalPersonYearsVaccinated) / float64(line.TotalPersonYearsVaccinated+line.TotalPersonYearsUnVaccinated) * 100
		unvaccinatedPercent := 100 - line.VaccinatedPercent
		ratioVacPercent := unvaccinatedPercent / line.VaccinatedPercent
		log.Println("Ratio non vacciné", ratioVacPercent)

		line.StandardizedCovidDeathVaccinated = ratioVacPercent * float64(line.TotalDeathVaccinated)
		line.StandardizedNoCovidDeathUnVaccinated = float64(line.TotalDeathUnVaccinated)

		totalDeath := line.StandardizedNoCovidDeathUnVaccinated + line.StandardizedCovidDeathVaccinated
		noVacPercentDeath := float64(line.StandardizedNoCovidDeathUnVaccinated / float64(totalDeath) * 100)
		vacPercentDeath := float64(line.StandardizedCovidDeathVaccinated / float64(totalDeath) * 100)
		log.Println("noVacPercentDeath", noVacPercentDeath)
		log.Println("vacPercentDeath", vacPercentDeath)
		line.CompareDeathPercent = (vacPercentDeath - noVacPercentDeath) / (noVacPercentDeath)
	}
}
