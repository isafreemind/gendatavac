package main

import (
	"strconv"
)

func runReworkAgeDemog() {
	ageDemog.adapteAgeForOns()

}

func (ageDemog AgeDemog) adapteAgeForOns() {
	for _, value := range ageDemog {
		key := strconv.Itoa(value.DateGo.Year()) + value.DateGo.Month().String() + mapAgeDemogToOns[value.Age]
		if _, ok := demog[key]; !ok {
			demog[key] = make(map[string]float64)
		}

		demog[key]["count"]++
		demog[key]["addpercent"] += value.CumVaccinationFirstDoseUptakeByVaccinationDatePercentage
	}

	for index, value := range ageDemog {
		key := strconv.Itoa(value.DateGo.Year()) + value.DateGo.Month().String() + mapAgeDemogToOns[value.Age]
		count := demog[key]["count"]
		addpercent := demog[key]["addpercent"]
		percent := addpercent / count
		demog[key]["percent"] = percent
		ageDemog[index].OneAgeGroupPercent = percent
	}
}
