package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"time"

	"github.com/gocarina/gocsv"
)

var (
	repAge []AgeRepartition
)

type AgeRepartition struct {
	AgeGroup string `csv:"Age group"`
	Mid2019  int    `csv:"Mid-2019"`
	Mid2020  string `csv:"Mid-2020"`
}

func loadAgeRepartition() {
	gocsv.SetCSVReader(func(in io.Reader) gocsv.CSVReader {
		r := csv.NewReader(in)
		r.Comma = ','
		return r // Allows use pipe as delimiter
	})
	csvFile, err := os.Open("input/demographie_uk.csv")
	printErr(err)
	defer csvFile.Close()
	bufio.NewReader(csvFile)
	gocsv.Unmarshal(csvFile, &onsData.Table8)
	for index := range onsData.Table8 {
		t, _ := time.Parse("2006 January", fmt.Sprintf("%d %s", onsData.Table8[index].Year, onsData.Table8[index].Month))
		onsData.Table8[index].DateGo = &t
	}
}

func saveAgeRepartition() {
	path := "output/rework_ons.csv"
	csvWrite, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0655)
	printErr(err)
	defer csvWrite.Close()
	writeThis, err := gocsv.MarshalBytes(&onsData.Table8)
	printErr(err)
	csvWrite.Write(writeThis)
	writeExcelFormat(writeThis, path)
}
